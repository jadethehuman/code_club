# Code Club
Presentations, Jupyter Notebooks and Code examples from talks in code club

## Introduction to Python
Based on the results of the questionnaire we will be starting at the very basics such as types, data structures and the python syntax. 
We will also be having a look at numpy and pandas in order to learn how to read, manipulate and write data, as well as some basic plotting with matplotlib.

* Jupyter Notebook with small example tasks
* Introductory slides
* Two data sets to explore